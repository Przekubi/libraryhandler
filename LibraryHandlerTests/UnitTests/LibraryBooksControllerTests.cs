﻿using LibraryHandler3.Controllers;
using LibraryHandler3.Models;
using LibraryHandler3.Repository.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;

namespace LibraryHandlerTests.UnitTests
{
    [TestClass]
    public class LibraryBooksControllerTests
    {
        IUnitOfWork fakeUnitOfWork;
        LibraryBooksController fakeController;
        [TestInitialize]
        public void Initialize()
        {
            fakeUnitOfWork = Substitute.For<IUnitOfWork>();
            fakeController = new LibraryBooksController(fakeUnitOfWork);
        }

        [TestMethod]
        public void GetBooks_BookReciveSuccesfully_ReturnOK()
        {
            //arrange
            IEnumerable<Book> books = Substitute.For<IEnumerable<Book>>();
            fakeUnitOfWork.Books.GetAll().Returns(books);

            //act
            var result = fakeController.GetBooks() as IHttpActionResult;

            //assert
            Assert.IsInstanceOfType(result, typeof(OkNegotiatedContentResult<IEnumerable<Book>>));
        }
    }
}
