﻿using LibraryHandler3.Controllers;
using LibraryHandler3.Data;
using LibraryHandler3.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace LibraryHandlerTests.UnitTests
{
    [TestClass]
    public class LibraryControllerTests
    {
        LibraryController fakeLibraryController;
        IHttpRestService fakeRestService;

        [TestInitialize]
        public void Initialize()
        {
            fakeRestService = Substitute.For<IHttpRestService>();
            fakeLibraryController = new LibraryController(fakeRestService);
            fakeLibraryController.Adress = "Test";
        }

        [TestMethod]
        public async Task Index_SendRequestAndReciveBooks_ReturnViewWithBooks()
        {
            //arrange
            IEnumerable<Book> fakeBooksList = Substitute.For<IEnumerable<Book>>();

            fakeRestService.GetBooks().Returns(fakeBooksList);

            //act
            var result = await fakeLibraryController.Index() as ViewResult;

            //assert
            Assert.AreEqual(result.Model, fakeBooksList);
        }

        [TestMethod]
        public async Task Index_SendRequestAndReciveNull_ReturnStatusCode()
        {
            //arrange
            IEnumerable<Book> fakeBooksList = null;
            var fakeStatusCode = HttpStatusCode.Forbidden;
            fakeRestService.StatusCode.Returns(fakeStatusCode);
            fakeRestService.GetBooks().Returns(fakeBooksList);

            //act
            var result = await fakeLibraryController.Index() as HttpStatusCodeResult;

            //assert
            Assert.AreEqual((HttpStatusCode)result.StatusCode, fakeStatusCode);
        }

        [TestMethod]
        public async Task Index_SendRequestAndThrowException_ReturnBadRequest()
        {
            //arrange
            fakeRestService.When(o=>o.GetBooks()).Throw(new Exception());

            //act
            var result = await fakeLibraryController.Index() as HttpStatusCodeResult;

            //assert
            Assert.AreEqual((HttpStatusCode)result.StatusCode, HttpStatusCode.BadRequest);

        }
    }
}
