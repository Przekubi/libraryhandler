﻿using LibraryHandler3.Data;
using LibraryHandler3.Models;
using LibraryHandler3.Repository.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace LibraryHandler3.Controllers
{
    public class LibraryController : Controller
    {
        IHttpRestService _restService;
        public LibraryController(IHttpRestService restService)
        {
            _restService = restService;
        }

        public string Adress;

        private string adress
        {
            get
            {
                if (String.IsNullOrEmpty(Adress))
                {
                    return Request.Url.Scheme + "://" + Request.Url.Authority + "/api/LibraryBooks/GetBooks";
                }
                else
                {
                    return Adress;
                }
            }
            set
            {
                Adress = adress;
            }

        }

        public async Task<ActionResult> Index()
        {
            _restService.Adress = adress;
            try
            {
                var books = await _restService.GetBooks();
                if (books != null)
                {
                    return View(books);
                }
                else
                {
                    return new HttpStatusCodeResult(_restService.StatusCode);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _restService.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}