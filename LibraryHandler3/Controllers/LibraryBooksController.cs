﻿using LibraryHandler3.Models;
using LibraryHandler3.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LibraryHandler3.Controllers
{
    public class LibraryBooksController : ApiController
    {
        IUnitOfWork _unitOfWork;
        public LibraryBooksController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        [HttpGet]
        public IHttpActionResult GetBooks()
        {
            return Ok(_unitOfWork.Books.GetAll());
        }
        [HttpPost]
        public IHttpActionResult AddBook(Book book)
        {
            _unitOfWork.Books.Add(book);
            try
            {
                _unitOfWork.SaveChanges();
            }
            catch
            {
                return BadRequest();
            }
            return Ok(book.BookId);
        }
        [HttpPut]
        public IHttpActionResult UpdateBook(Book book)
        {
            _unitOfWork.Books.Edit(book);
            try
            {
                _unitOfWork.SaveChanges();
            }
            catch
            {
                return BadRequest();
            }
            return Ok();
        }
        [HttpDelete]
        public IHttpActionResult DeleteBook(int id)
        {
            var book = _unitOfWork.Books.Find(id);
            if (book==null)
            {
                return NotFound();
            }
            _unitOfWork.Books.Delete(book);
            _unitOfWork.SaveChanges();
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
