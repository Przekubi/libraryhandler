﻿using LibraryHandler3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace LibraryHandler3.Data
{
    public class HttpRestService : IHttpRestService
    {
        HttpClient client;
        public HttpRestService()
        {
            client = new HttpClient();
        }

        public HttpStatusCode StatusCode { get; private set; }
        public string Adress { private get; set; }

        public async Task<IEnumerable<Book>> GetBooks()
        {
            //var adress = Request.Url.Scheme + "://" + Request.Url.Authority + "/api/LibraryBooks/GetBooks";
            var uri = new Uri(Adress);
            HttpResponseMessage response = null;
            try
            {
                response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    IEnumerable<Book> books = Newtonsoft.Json.JsonConvert.DeserializeObject<IEnumerable<Book>>(content);
                    StatusCode = HttpStatusCode.OK;
                    return books;
                }
                else
                {
                    return null;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public void Dispose()
        {
            if (client != null)
            {
                client.Dispose();
            }
        }

    }
}