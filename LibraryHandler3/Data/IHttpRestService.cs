﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using LibraryHandler3.Models;

namespace LibraryHandler3.Data
{
    public interface IHttpRestService
    {
        string Adress { set; }
        HttpStatusCode StatusCode { get; }
        
        Task<IEnumerable<Book>> GetBooks();

        void Dispose();
    }
}