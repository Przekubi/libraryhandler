﻿using LibraryHandler3.Models;
using LibraryHandler3.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryHandler3.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly LibraryHandlerContext _context;

        public UnitOfWork(LibraryHandlerContext context)
        {
            _context = context;
            Books = new BookRepository(_context);
        }

        public IBookRepository Books { get; private set; }

        public int SaveChanges()
        {
            try
            {
                return _context.SaveChanges();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 0;
            }
        }
        public void Dispose()
        {
            if (_context != null)
            {
                _context.Dispose();
            }
        }
    }
}