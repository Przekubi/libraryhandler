﻿namespace LibraryHandler3.Repository.Interfaces
{
    public interface IUnitOfWork
    {
        IBookRepository Books { get; }

        void Dispose();
        int SaveChanges();
    }
}