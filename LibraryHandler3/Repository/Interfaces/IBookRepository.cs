﻿using LibraryHandler3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryHandler3.Repository.Interfaces
{
    public interface IBookRepository : IRepository<Book>
    {
    }
}
