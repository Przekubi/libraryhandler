﻿using LibraryHandler3.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace LibraryHandler3.Repository
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly DbContext Context;

        public Repository(DbContext context)
        {
            Context = context;
        }
        public virtual IEnumerable<TEntity> GetAll()
        {
            IQueryable<TEntity> query = Context.Set<TEntity>();
            return query.ToList();
        }


        public virtual TEntity Find(params object[] keyValues)
        {
            TEntity query = Context.Set<TEntity>().Find(keyValues);
            return query;
        }

        public virtual void Add(TEntity entity)
        {

            Context.Set<TEntity>().Add(entity);

        }

        public virtual void Delete(TEntity entity)
        {
            Context.Set<TEntity>().Remove(entity);
        }
        public virtual void Edit(TEntity entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
        }


    }
}